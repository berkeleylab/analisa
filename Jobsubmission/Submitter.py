#! /usr/bin/env python
# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
Module running on the submitter side.
Parses the job definition and translates them into worker side job parameters,
which contain additional information
Furthermore it produces for the worker
- the master executable
- the local sandbox
The cluster is auto-detected from the name of the submitter host

:since: Oct 15, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
"""

import getopt, os, sys, shutil

import json as jsonhandler
from Jobsubmission.Jobparameters import Jobparams
from Jobsubmission.JobDefinition import JobDefinition
from Jobsubmission.MasterJobDefinition import MasterJobDefinition
from Jobsubmission.clusters import cori, edison
    
class ConfigWriterFactory(object):
    
    @staticmethod
    def GetConfigWriter(mode):
        if mode == "file":
            return FileConfigWriter()
            
class ConfigWriter(object):
    
    def __init__(self):
        self._entrylist = {}
        
    def AddObject(self, key, value, isPrimitive = False):
        self._entrylist[key] = value if isPrimitive else value.MakeDict()
    
class FileConfigWriter(ConfigWriter):
    
    def __init__(self):
        ConfigWriter.__init__(self)
    
    def Write(self, filename):
        configfile = open(filename, "w")
        configfile.write("%s\n" %(jsonhandler.dumps(self._entrylist)))
        configfile.close()
        
class PackageHandler(object):
    
    def __init__(self, sandbox):
        self.__sandbox = sandbox
        self.__packages = []
        if not os.path.exists(self.__sandbox):
            os.makedirs(self.__sandbox, 0755)
        # Create also module path
        moduledir = os.path.join(self.__sandbox, "modulefiles")
        if not os.path.exists(moduledir):
            os.makedirs(moduledir, 0755)
        self.InstallBaseModule()       
        
    def GetModulePath(self):
        return os.path.join(self.__sandbox, "modulefiles")
    
    def GetListOfPackages(self):
        return self.__packages
            
    def InstallBaseModule(self):
        template = "/project/projectdirs/alice/hpc/packages/modulefiles/templates/base_1.0"
        outputdir = os.path.join(self.__sandbox, "modulefiles", "BASE")
        if not os.path.exists(outputdir):
            os.makedirs(outputdir, 0755)
        
        templatereader = open(template)
        basewriter = open("%s/1.0" %outputdir, 'w')
        for line in templatereader:
            if "{BASEPATH}" in line:
                line = line.replace("{BASEPATH}", self.__sandbox)
            basewriter.write(line)
        templatereader.close()
        basewriter.close()
        self.__packages.append("BASE/1.0")
    
    def InstallPackage(self, packagename):
        print "Installing package %s" %packagename
        inputdir = "/project/projectdirs/alice/hpc/packages/%s" %(os.environ["NERSC_HOST"])
        packagetar = "%s.tar.gz" %(os.path.basename(packagename)) 
        packageinput = "%s/%s.tar.gz" %(inputdir, packagename)
        packagecategory = os.path.dirname(packagename)
        if os.path.exists(packageinput):
            packagedir = os.path.join(self.__sandbox, packagecategory)
            if not os.path.exists(packagedir):
                os.makedirs(packagedir, 0755)
            shutil.copy(packageinput, packagedir)
            pwd = os.getcwd()
            os.chdir(packagedir)
            os.system("tar xzf %s" %packagetar)
            os.remove(os.path.join(packagedir, packagetar))

            # install modulefile 
            modulecategory = os.path.dirname(packagename)
            moduleinput = "/project/projectdirs/alice/hpc/packages/modulefiles/%s/%s" %(os.environ["NERSC_HOST"], packagename)
            moduleout = os.path.join(self.__sandbox, "modulefiles", modulecategory)
            if not os.path.exists(moduleout):
                os.makedirs(moduleout, 0755)
            shutil.copy(moduleinput, moduleout)
            os.chdir(pwd)
            
            self.__packages.append(packagename)
    

class MasterJob(object):
    
    def __init__(self, cluster, masterjobparams):
        self.__cluster = cluster
        self.__masterjobparams= masterjobparams
        self.__jobparams = None
        self.__configdata = None
        self.__debugMode = False
        self.__jobid = 0
        
    def SetJobParams(self, params):
        self.__jobparams = params
        
    def SetDebugMode(self):
        self.__cluster.SetDebugMode()
        
    def WriteConfig(self, mode):
        writer = ConfigWriterFactory.GetConfigWriter(mode)
        writer.AddObject("jobparams", self.__jobparams)
        writer.AddObject("sandbox", self.__masterjobparams.GetGlobalSandbox().GetLocation(), isPrimitive = True)
        param = ""
        if mode == "file":
            param = "%s/jobconfig.json" %(self.__masterjobparams.GetGlobalSandbox().GetLocation())
        writer.Write(param)
        self.__configdata = {"mode":mode, "param":param}

    def GenerateSteeringScript(self):
        """
        Generate jobscript for MPI master
        """
        print "Generating MPI Wrapper for cluster %s" %(self.__masterjobparams.GetCluster())
        self.__cluster.GenerateMasterScript(self.__masterjobparams, self.__configdata)
        
    def Submit(self):
        self.__cluster.SubmitMasterJob(self.__masterjobparams.GetGlobalSandbox().GetLocation())
                
    def GetJobID(self):
        return self.__cluster.GetJobID()
    
def main():
    try:
        opt, arg = getopt.getopt(sys.argv[2:], "dr:")
    except getopt.GetoptError as e:
        print str(e)
        sys.exit(1)
        
        
    jdl = sys.argv[1]

    clusterClass = None
    nerscsystem = os.getenv("NERSC_HOST")
    if "edison" in nerscsystem:
        clusterClass = edison.edison()
    elif "cori" in nerscsystem:
        clusterClass = cori.cori()
    else:
        print "System unsupported"
        sys.exit(1)
    print "Submitting jobs on cluster %s" %clusterClass.GetName()
        
    jobdef = JobDefinition()
    if str(jdl).endswith(".json"):
        jobdef.BuildFromJSONFile(jdl)
    elif str(jdl).endswith(".xml"):
        jobdef.BuildFromXMLFile(jdl)
    else:
        jobdef.BuildFromConfigFile(jdl)

    rundebug = False
    for o, a in opt:
        if o == "-d":
            rundebug = True
            
    if len(jobdef.GetCluster()) and clusterClass.GetName() != jobdef.GetCluster():
        print "Cluster not matching the expectation: Selected %s, running %s" %(jobdef.GetCluster(), clusterClass.GetName())
        sys.exit(1)
        
    # Create global output directory already at job submission level in order
    # to be able to write the logfile immediately there.
    if not os.path.exists(jobdef.GetOutputBase()):
        os.makedirs(jobdef.GetOutputBase(), 0755)
        
    # handle packages 
    globalsandbox = "%s/%s" %(clusterClass.GetSandboxLocation(), jobdef.GetJobname())
    packagemodules = []
    packagemodulepaths = []
    if len(jobdef.GetPackages()):
        packman = PackageHandler("%s/packages" %globalsandbox)
        packagemodulepaths.append(packman.GetModulePath())
        for pack in jobdef.GetPackages():
            packman.InstallPackage(pack)
        for pack in packman.GetListOfPackages():
            packagemodules.append(pack)
    
    # handle pythonpath        
    scriptpath = os.path.realpath(os.path.dirname(sys.argv[0]))
    pythonpath = scriptpath.replace("/Jobsubmission", "")
            
    jobids = []
    nmaster = clusterClass.CalculateNumberOfMasterjobs(jobdef.GetNJobs())
    currentmin = 0
    for masterID in range(0, nmaster):
        jpar = Jobparams()
        ppar = MasterJobDefinition()
        ppar.SetCluster(clusterClass.GetName())
        ppar.SetPYTHONPATH(pythonpath)

        for outputgroup in jobdef.GetOutputfiles():
            jpar.AddOutputGroup(outputgroup)
            
        sandboxdir = "%s/%s/master%d" %(clusterClass.GetSandboxLocation(), jobdef.GetJobname(), masterID)
        print "Creating sandbox in %s" %(sandboxdir)
        if os.path.exists(sandboxdir):
            print "Output location already existing"
            sys.exit(1)
        ppar.SetGlobalsandbox(sandboxdir)
        if not ppar.GetGlobalSandbox().IsCreated():
            print "Global sandbox for the master job needs to be specified"
            sys.exit(1)
    
        jpar.SetOutputLocation(jobdef.GetOutputBase())
        jpar.SetExecutable(jobdef.GetExecutable())
        jpar.SetExecutableArgs(jobdef.GetExecutableArgs())
        jpar.SetValidation(jobdef.GetValidation())
        ppar.SetJobname(jobdef.GetJobname())
        ppar.SetOutputLocation(jobdef.GetOutputBase())

        # calculate number of workers per parallel job
        nparallel = clusterClass.GetCoresPerNode()
        if jobdef.GetNJobs() - currentmin < nparallel:
            nparallel = jobdef.GetNJobs() - currentmin
        ppar.SetNParallel(nparallel)
        ppar.SetQueue(jobdef.GetQueue() if len(jobdef.GetQueue()) else clusterClass.GetDefaultQueue())
        ppar.SetTimeLimit(jobdef.GetTimeLimit() if len(jobdef.GetTimeLimit()) else clusterClass.GetDefaultTime())

        if len(packagemodulepaths):
            for mod in packagemodulepaths:
                jpar.AddUserModulepath(mod)
        if len(packagemodules):
            for mod in packagemodules:
                jpar.LoadUserModule(mod)
        if len(jobdef.GetInputfiles()):
            for infi in jobdef.GetInputfiles():
                jpar.AddInputfile(infi)
        if len(jobdef.GetModulePaths()):
            for path in jobdef.GetModulePaths():
                jpar.AddUserModulepath(path)
        if len(jobdef.GetUserModules()):
            for mod in jobdef.GetUserModules():
                jpar.LoadUserModule(mod)
        
        mymaster = MasterJob(clusterClass, ppar)
        jpar.SetMasterJobID(masterID)
        jpar.SetGlobalJobRange(currentmin, currentmin + nparallel - 1)
        if rundebug:
            mymaster.SetDebugMode()
        mymaster.SetJobParams(jpar)
        mymaster.WriteConfig("file")
        mymaster.GenerateSteeringScript()
        mymaster.Submit()
        jobids.append(mymaster.GetJobID())
        currentmin += nparallel
        
    # Write all master job IDs to a file in the global sandbox
    jobidfile = open("%s/jobids" %globalsandbox, "w")
    for jobid in jobids:
        jobidfile.write("%d\n" %jobid)
    jobidfile.close()

    # Launch garbage collection
    #os.system("%s/Jobsubmission/GarbageCollection.py --start --sandbox=%s --file=%s/jobids" %(pythonpath, globalsandbox, globalsandbox))

if __name__ == "__main__":
        main()
