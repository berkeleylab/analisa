# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
Modules for job specific params
Providing an exchange interface based on JSON

:since: Oct, 13, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''
import json as jsonhandler
from Jobsubmission.OutputFileGroup import OutputFileGroup

class Jobparams(object):
    """
    Job parameters (like JDL)
    """
    
    def __init__(self):
        self.__executable = "aliroot"
        self.__executableArgs = ""
        self.__validatation = ""
        self.__modulepaths = []
        self.__inputfiles = []
        self.__modules = []
        self.__outputgroups = []
        self.__outputlocaltion = ""
        self.__masterID = 0
        self.__jobcounter = {"min":0, "max":0}
        
    def AddUserModulepath(self, modulepath):
        self.__modulepaths.append(modulepath)
        
    def AddInputfile(self, inputfile):
        self.__inputfiles.append(inputfile)
        
    def AddInputfilesToSandbox(self, target):
        for infi in self.__inputfiles:
            target.AddInputObject(infi)
            
    def AddOutputGroup(self, mygroup):
        self.__outputgroups.append(mygroup)
        
    def LoadUserModule(self, modulename):
        self.__modules.append(modulename)
        
    def SetExecutable(self, executable):
        self.__executable = executable
        
    def SetExecutableArgs(self, args):
        self.__executableArgs = args
        
    def SetValidation(self, args):
        self.__validatation = args
        
    def SetMasterJobID(self, jobid):
        self.__masterID = jobid
        
    def SetGlobalJobRange(self, jobmin, jobmax):
        self.__jobcounter["min"] = jobmin
        self.__jobcounter["max"] = jobmax
        
    def SetOutputLocation(self, location):
        self.__outputlocaltion = location
                    
    def GetExecutable(self):
        return self.__executable
    
    def GetExecutableArgs(self):
        return self.__executableArgs
    
    def GetValidation(self):
        return self.__validatation
        
    def GetUserModules(self):
        return self.__modules
    
    def GetUserModulePaths(self):
        return self.__modulepaths

    def GetInputfiles(self):
        return self.__macros
    
    def GetOutputLocation(self):
        return self.__outputlocaltion
    
    def GetOutputGroups(self):
        return self.__outputgroups
    
    def GetMasterJobID(self):
        return self.__masterID
    
    def GetJobIDLimits(self):
        return self.__jobcounter    
    
    def GetMinJobID(self):
        return self.__jobcounter["min"]
    
    def GetMaxJobID(self):
        return self.__jobcounter["max"]
    
    def HasValidation(self):
        return (len(self.__validatation) != 0)
    
    def __repr__(self, *args, **kwargs):
        return self.MakeDict()
    
    def MakeDict(self):
        """
        Serialise to dictionary
        """
        outputgroups = None
        if len(self.__outputgroups):
            outputgroups = []
            for mygroup in self.__outputgroups:
                outputgroups.append(mygroup.MakeDict())
        result = {"executable":self.__executable, "arguments":self.__executableArgs, "validation":self.__validatation, \
                "outputpath":self.__outputlocaltion, "inputfiles":self.__inputfiles, "usermodulepaths":self.__modulepaths, \
                "usermodules": self.__modules, "masterjob":self.__masterID, "jobid":self.__jobcounter}
        if outputgroups:
            result["outputgroups"] = outputgroups
        return result
        
    def __getstate__(self):
        return self.MakeDict()
    
    def __setstate__(self, mydict):
        self.__init__()
        self.InitialiseFromDict(mydict)
        
    def Print(self):
        print "Job parameters:"
        print "=============================================="
        print "Executable: %s" %(self.__executable)
        print "Arguments %s" %(self.__executableArgs)
        print "MasterJob: %d (Job range [%d,%d])" %(self.__masterID, self.__jobcounter["max"]. self.__jobcounter["max"])
        print "Output directory: %s" %(self.__outputlocaltion)
        if len(self.__macros):
            print "User inputfiles:"
            for macroname in self.__inputfiles:
                print macroname
        if len(self.__modulepaths):
            print "User defined module paths:"
            for mypath in self.__modulepaths:
                print mypath
        if len(self.__modules):
            print "User defined modules:" 
            for mymod in self.__modules:
                print mymod   
        print "Output files:"
        print "-------------------------------"
        print "=============================================="

    """
    Handling for JSON for IO
    """
    
    def CreateJSONString(self):
        """
        Create a JSON-serialisation of the jobparams object
        """
        return jsonhandler.dumps(self.MakeDict())
    
    def InitialiseFromJSON(self, jsonstring):
        """
        Initialise Object with values from jsonstring
        """
        objects = jsonhandler.loads(jsonstring)
        self.InitialiseFromDict(objects)
        
    def InitialiseFromDict(self, objdict):
        for objname, objval in objdict.iteritems():
            if objname == "executable":
                self.__executable = objval
            elif objname == "validation":
                self.__validation = objval
            elif objname == "arguments":
                self.__executableArgs = objval
            elif objname == "masterjob":
                self.__masterID = objval
            elif objname == "outputpath":
                self.__outputlocaltion = objval
            elif objname == "jobid":
                for limit in ["min", "max"]:
                    self.__jobcounter[limit] = objval[limit]
            elif objname == "inputfiles":
                for macroname in objval:
                    self.__inputfiles.append(macroname)
            elif objname == "usermodulepaths":
                for modpath in objval:
                    self.AddUserModulepath(modpath)
            elif objname == "usermodules":
                for modname in objval:
                    self.__modules.append(modname)
            elif objname == "outputgroups":
                for f in objval:
                    mygroup = OutputFileGroup()
                    mygroup.FromDict(f)
                    self.__outputgroups.append(mygroup)
            else:
                print "Unknown attribute: %s" %(objname)
    
    @staticmethod
    def BuildFromJSON(jsonstring):
        """
        Static method initialising an object from a jsonstring
        """
        result = Jobparams()
        result.InitialiseFromJSON(jsonstring)
        return result
    
    @staticmethod
    def FillFromJSON(newparams, jsonstring):
        """
        Method initialising an existing object with values from a
        json string
        """
        newparams.InitialiseFromJSON(jsonstring)
    
