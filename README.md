The Analisa tool submitting multiple serial jobs to MPI clusters
================================================================

Job submission (currently focused on simulation jobs) is steer via the ALICE production system (ANAliSA). For this the module "alice/analisa" needs to be loaded. For job submission one needs:

* A config.ali file
* An executable script
* Macros one needs to process
* Pre-compiled software or packages

For performance reasons users should install their software under $SCRATCH. Keep in mind that $SCRATCH is not a long term file system and files will be deleted after 30 days. For recent aliroot versions pre-compiled packages will be provided. They will be installed for each analisa job.
In the configfile users need to define

* a base directory where the output is stored (key outputbase)
* an executable (key executable)
* a validation script (key validation)
* (optional) a set of arguments for the execution script (key arguments)
    Either:
    * A list of pre-compiled packages (key packages)
    * A list of module paths (key modulepaths) and modules (key modules) where the user takes care of
* The number of jobs requested (key njobs). Job splitting will be done in multiples of 24
* A time limit after which the job will be killed (key timelimit). On HPC machines this setting is essential. Plan it well. ":" has to be replaced by "."
* The queue on which the job should run (key queue). For normal purpose the queue "regular" should be sufficient.
* A name of the job (key jobname). Also this parameter is essential

The configuration is a key-value pair, where the value can be a comma-separated list where applicable (modulefiles, packages, inputfiles, outputfiles). Several outputfiles can be grouped to a root_archive using the @-operator. The operator #jobid can be used as a blueprint for the interal job counter. In the executable, users can the jobid as well via the environment variable JOB_ID.
An example config looks like this:

    outputbase:  /project/projectdirs/alice/mfasel/ppfast/PYTHIA8PtHard/result_pilot_test
    executable:  /project/projectdirs/alice/mfasel/ppfast/PYTHIA8PtHard/source/runsim.sh
    validation:  /project/projectdirs/alice/mfasel/ppfast/PYTHIA8PtHard/source/validation.sh
    arguments:   "--event #jobid --pthardbin 1 --nevents 100"
    packages:    ROOT/v5-34-08, GEANT3/v1-15a_rootv5-34-08, AliRoot/master
    inputfiles:  /project/projectdirs/alice/mfasel/ppfast/PYTHIA8PtHard/source/simana.C
    outputfiles: histos.root@root_archive.zip;sim.log,worker.log@log_archive.zip
    njobs:       24
    timelimit:   2.00.00
    queue:       regular
    jobname:     PYTHIA8PtHardPilot_test

The submitter takes care about creating the working directory, installing the packages and loading the modules. Users must not do this in their executable!
For job submission one simply does

`analisa submit -c config.ali`