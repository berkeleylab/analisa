# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Nov 26, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

import os
from Jobsubmission.Outputarchive import OutputArchiveFactory
from shutil import copy as copycontent

class OutputFileGroup(object):
    """
    General group of files (including regular expressions in filenames)
    """


    def __init__(self):
        '''
        Constructor
        '''
        self.__archive = ""
        self.__patterns = []
        self.__writeOnFailiure = True
        
    def BuildFromString(self, content):
        """
        Parse string from the job description, and build output group accordingly
        """
        if "(writeOnFail)" in content:
            self.SetWriteOnFailure(True)
            content = content.replace("(writeOnFail)","")
        if "(nowriteOnFail)" in content:
            self.SetWriteOnFailure(True)
            content = content.replace("(nowriteOnFail)","")
        # Find Archive
        if "@" in content:
            self.__archive = content[content.index("@")+1:]
            content = content.replace("@%s" %(self.__archive), "")
        # Find patterns
        self.__patterns = content.split(",")
        
    def SetWriteOnFailure(self, doWrite = True):
        self.__writeOnFailiure = doWrite
        
    def SetArchive(self, archivename):
        self.__archive = archivename
        
    def AddPattern(self, pattern):
        self.patterns.append(pattern)
        
    def GetListOfPatterns(self):
        return self.__patterns
    
    def IsWriteOnFailure(self):
        return self.__writeOnFailiure
    
    def HasArchive(self):
        return (len(self.__archive) != 0)
    
    def CheckSandboxForPatterns(self, sandbox):
        """
        Check whether we find all files, or files which follow our regular expression
        inside the sandbox
        """
        status = {}
        for pattern in self.__patterns:
            status[pattern] = True if len(sandbox.GetFiles(pattern)) else False
        return status
            
    def CheckSandboxForArchive(self, sandbox):
        return os.path.exists("%s/%s" %(sandbox.GetLocation(), self.__archive))
    
    def CreateArchive(self, sandbox):
        """
        Create output archive if specified
        """
        if len(self.__archive):
            sandbox.Refresh()
            filelist = []
            for outpattern in self.__patterns:
                matched = sandbox.GetFiles(outpattern)
                for foundfile in matched:
                    filelist.append(foundfile)
                archive = OutputArchiveFactory.CreateOutputArchive(self.__archive, filelist)
            print "Compression to %s: %s" %(self.__archive, "Success" if archive.GetStatus() else "Failure")
            
    def WriteToFinalLocation(self, sandbox, outputloc):
        """
        Move content from the sandbox to its final location
        If content is supposed to be compressed, compress it, otherwise move files separately
        """
        if self.__archive:
            if self.CheckSandboxForArchive(sandbox):
                copycontent("%s/%s" %(sandbox.GetLocation(), self.__archive), outputloc)
        else:
            # Copy all outputfiles separately
            for mypattern in self.__patterns:
                listoffound = sandbox.GetFiles(mypattern) 
                for found in listoffound:
                    copycontent("%s/%s" %(sandbox, found), outputloc)

    def MakeDict(self):
        """
        Serialise to dictionary
        """
        return {"archive":self.__archive, "patterns":self.__patterns, "writeOnFailure":self.__writeOnFailiure}
    
    def FromDict(self, mydict):
        """
        Rebuild from dictionary
        """
        self.__archive = mydict["archive"]
        self.__patterns = mydict["patterns"]
        self.__writeOnFailiure = mydict["writeOnFailure"]
        
    def __getstate__(self):
        return self.MakeDict()
    
    def __setstate__(self, mydict):
        self.__init__()
        self.InitialiseFromDict(mydict)