# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Jun 8, 2015
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''

import commands

class HPCJobInfo(object):
    
    def __init__(self, jobid, user, jobname, queue, session, nnodes, ncores, memorylimit, timelimit, status, timeelapsed):
        jobidtmp = jobid
        if "." in jobidtmp:
            jobidtmp = jobid[0:jobidtmp.find(".")]
        self.__jobid = int(jobidtmp)
        self.__username = user
        self.__jobname = jobname
        self.__queue = queue
        self.__session = session
        self.__numberofnodes = nnodes
        self.__numberofcores = ncores
        self.__memlimit = memorylimit
        self.__timelimit = timelimit
        self.__timeelapsed = timeelapsed
        self.__status = status
        
    def __cmp__(self, other):
        if isinstance(other, int):
            return self.__CompareJobID(int(other))
        elif isinstance(other, HPCJobInfo):        
            return self.__CompareJobID(other.GetJobId())
        else:
            # Cannot compare
            return 1
            
    def __CompareJobID(self, otherid):
        if self.__jobid == otherid:
            return 0
        elif self.__jobid > otherid:
            return 1
        else:
            return -1
        
    def GetJobId(self):
        return self.__jobid
    
    def GetJobname(self):
        return self.__jobname
    
    def GetJobStatus(self):
        return self.__status
    
    def GetEdisonUsername(self):
        return self.__username
    
    def GetSessionID(self):
        return self.__session
    
    def GetNumberOfNodes(self):
        return self.__numberofnodes
    
    def GetNumberOfCores(self):
        return self.__numberofcores
    
    def GetMemoryLimit(self):
        return self.__memlimit
    
    def GetTimeLimit(self):
        return self.__timelimit
    
    def GetTimeElapsed(self):
        return self.__timeelapsed
    
class HPCJobList(object):
    
    def __init__(self):
        self.__jobs = []
        self.__item = 0
        
    def __iter__(self):
        self.__item = 0
        return self
    
    def __next__(self):
        try:
            result = self.__jobs[self.__item]
        except IndexError:
            raise StopIteration
        self.__item += 1
        return result
    
    def next(self):
        return self.__next__()

    def InsertJob(self, job):
        if not job in self.__jobs:
            self.__jobs.append(job)
            
    def FindJob(self, jobid):
        if jobid in self.__jobs:
            return self.__jobs[self.__jobs.index(jobid)]
        return None
    
class HPCJobHandler(object):
    
    def __init__(self):
        pass
    
    def GetJobListForUser(self, username):
        result = HPCJobList()
        jobs = commands.getstatusoutput("qstat -u %s" %username)[1].split("\n")
        for job in jobs:
            if not username in job:
                continue
            tokens = self.__TokenizeJobInfo(job)
            result.InsertJob(HPCJobInfo(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], tokens[6],
                                           tokens[7], tokens[8], tokens[9], tokens[10]))
        return result
    
    def __TokenizeJobInfo(self, line):
        result = []
        tst = line.replace(" ",";")
        while tst.find(";") >= 0:
            index = tst.find(";")
            result.append(tst[0:index])
            tst = tst[index+1:]
            # remove other delimiters
            if len(tst):
                while tst[0] == ";":
                    tst = tst[1:]
        # add last token to the list of tokens
        if len(tst):
            result.append(tst)
        return result
    
