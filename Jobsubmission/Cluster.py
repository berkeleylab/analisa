# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

'''
Created on 21.12.2015

@author: markusfasel
'''

class Cluster(object):
    '''
    classdocs
    '''

    def __init__(self):
        self._name = ""
        self._sandboxLocation = ""
        self._defaultQueue = ""
        self._defaultTime = ""
        self._jobid = 0
        self._debugMode = False
        self._corespernode = 0

    def GetCoresPerNode(self):
        return self._corespernode
    
    def CalculateNumberOfMasterjobs(self, numberofjobs):
        """
        Calculate number of masterjobs needed for a requested split level on
        job counts
        """
        numberofmasters = int(numberofjobs) / int(self._corespernode)
        if numberofjobs % self._corespernode != 0:
            numberofmasters += 1
        return numberofmasters 
    
    def GenerateMasterScript(self, jobdefinition, workerconfig):
        pass
     
    def SubmitMasterJob(self, sandbox):
        pass
    
    def GetMasterJobDefintion(self):
        return self._jobdefinition
    
    def GetJobID(self):
        return self._jobid
        
    def GetName(self):
        return self._name
    
    def GetSandboxLocation(self):
        return self._sandboxLocation
    
    def GetDefaultQueue(self):
        return self._defaultQueue
    
    def GetDefaultTime(self):
        return self._defaultTime
        
    def SetDebugMode(self):
        self._debugMode = True
        
