# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
Created on 21.12.2015

@author: markusfasel
'''
from Jobsubmission.OutputFileGroup import OutputFileGroup
import xml.etree.ElementTree as ET
import json

class JobDefinition(object):
    """
    Job definition class with all information provided 
    by the user to launch the job
    """
    
    def __init__(self):
        """
        Constructor
        """
        self.__cluster = ""
        self.__executable = ""
        self.__executableArgs = ""
        self.__validation = ""
        self.__inputfiles = []
        self.__jobname = ""
        self.__nevents = 0
        self.__njobs = 0
        self.__nmasterjob = 1
        self.__modulepaths = []
        self.__modules = []
        self.__packages = []
        self.__outputbase = ""
        self.__outputfiles = []
        self.__timelimit = ""
        self.__queue = ""
        
    def BuildFromConfigFile(self, filename):
        """
        Build job definition from any configuration file
        """
        configfile = open(filename)
        for line in configfile:
            self.__HandleLine(line.replace("\n",""))
        configfile.close()
        
    def BuildFromXMLFile(self, filename):
        """
        Build job definition from XML file
        """
        xmltree = ET.parse(filename)
        root = xmltree.getroot()
        keysToCheck = ["executable", "validation", "arguments", "outputbase", "jobname", "njobs", "queue", "cluster", "timelimit"]
        for mykey in keysToCheck:
            value = root.find(mykey)
            if value:
                if mykey == "executable":
                    self.__executable = value
                elif mykey == "validation":
                    self.__validation = value
                elif mykey == "arguments":
                    self.__executableArgs = value
                elif mykey == "outputbase":
                    self.__outputbase = value
                elif mykey == "jobname":
                    self.__jobname = value 
                elif mykey == "njobs":
                    self.__njobs = int(value)
                elif mykey == "queue":
                    self.__queue = value    
                elif mykey == "timelimit":
                    self.__timelimit = value

        infiles = root.findall("inputfile")
        for infile in infiles:
            self.__inputfiles.append(infile)
        modpaths = root.findall("modulepath")
        for mypath in modpaths:
            self.__modulepaths.append(mypath)
        modfiles = root.findall("modulefile")
        for myfile in modfiles:
            self.__modules.append(myfile)
        outfiles = root.findall("outputfile")
        for outfile in outfiles:
            outputgroup = OutputFileGroup()
            archive =  outfile.find("archive")
            if archive:
                outputgroup.SetArchive(archive)
            files = outfile.findall("file")
            for f in files:
                outputgroup.AddPattern(f)
            self.__outputfiles.append(outputgroup)
        

    def BuildFromJSONFile(self, filename):
        """
        Build job definition from JSON file
        """
        jsonstring = ""
        reader = open(filename, 'r')
        for line in reader:
            line = line.replace("\n", "").lstrip().strip()
            jsonstring += line
        self.BuildFromJSONString(jsonstring)
    
    def BuildFromJSONString(self, jsonstring):
        """
        Build job definition from JSON string.
        Can be used as interface to JSON file or MongoDB
        """
        jsontree = json.loads(jsonstring)
        for key,value in jsontree.iteritems():
            if key == "executable":
                self.__executable = value
            elif key == "validation":
                self.__validation = value
            elif key == "arguments":
                self.__executableArgs = value
            elif key == "outputbase":
                self.__outputbase = value
            elif key == "jobname":
                self.__jobname = value
            elif key == "njobs":
                self.__njobs = int(value)
            elif key == "queue":
                self.__queue = value
            elif key == "cluster":
                self.__cluster = value
            elif key == "timelimit":
                self.__timelimit = value
            elif key == "modulepaths":
                for path in value:
                    self.__modulepaths.append(path)
            elif key == "modules":
                for mod in value:
                    self.__modules.append(mod)
            elif key == "packages":
                for pack in value:
                    self.__packages.append(pack)
            elif key == "inputfiles":
                for mac in value:
                    self.__inputfiles.append(mac)
            elif key == "outputfiles":
                for mygroup in value:
                    outgroup = OutputFileGroup()
                    outgroup.SetArchive(mygroup["archive"])
                    for f in mygroup["files"]:
                        outgroup.AddPattern(f)
                    self.__outputfiles.append(outgroup)
        
    def __HandleLine(self, line):
        """
        Parse tokens from the config file
        """
        tokens = line.split(":")
        key = tokens[0]
        value = tokens[1]
        if key == "executable":
            self.__executable = value.replace(" ", "")
        if key == "validation":
            self.__validation = value.replace(" ", "")
        elif key == "arguments":
            self.__executableArgs = value
        elif key == "outputbase":
            self.__outputbase = value.replace(" ", "")
        elif key == "jobname":
            self.__jobname = value.replace(" ", "")
        elif key == "njobs":
            self.__njobs = int(value.replace(" ", ""))
        elif key == "queue":
            self.__queue = value.replace(" ", "")
        elif key == "cluster":
            self.__cluster = value.replace(" ", "")
        elif key == "timelimit":
            self.__timelimit = value.replace(".",":").replace(" ", "")
        elif key == "modulepaths":
            for path in value.split(","):
                self.__modulepaths.append(path.replace(" ", ""))
        elif key == "modules":
            for mod in value.split(","):
                self.__modules.append(mod.replace(" ", ""))
        elif key == "packages":
            for pack in value.split(","):
                self.__packages.append(pack.replace(" ", ""))
        elif key == "inputfiles":
            for mac in value.split(","):
                self.__inputfiles.append(mac.replace(" ", ""))
        elif key == "outputfiles":
            for outf in value.split(";"):
                outf = outf.replace(" ", "")
                mygroup = OutputFileGroup()
                mygroup.BuildFromString(outf)
                self.__outputfiles.append(mygroup)
                
    def GetExecutable(self):
        return self.__executable
    
    def GetExecutableArgs(self):
        return self.__executableArgs
    
    def GetValidation(self):
        return self.__validation

    def GetOutputBase(self):
        return self.__outputbase
    
    def GetMacroPath(self):
        return self.__macrolocation
    
    def GetNumberOfEvents(self):
        return self.__nevents
    
    def GetNJobs(self):
        return self.__njobs
    
    def GetJobname(self):
        return self.__jobname
    
    def GetInputfiles(self):
        return self.__inputfiles
    
    def GetModulePaths(self):
        return self.__modulepaths
    
    def GetUserModules(self):
        return self.__modules
    
    def GetPackages(self):
        return self.__packages
    
    def GetCluster(self):
        return self.__cluster
    
    def GetQueue(self):
        return self.__queue
    
    def GetTimeLimit(self):
        return self.__timelimit
    
    def GetOutputfiles(self):
        return self.__outputfiles