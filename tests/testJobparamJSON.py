# Copyright (c) 2015, Lawrence Berkeley National Laboratory
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list 
#    of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list 
#    of conditions and the following disclaimer in the documentation and/or other materials 
#    provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER 
# OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''
:since: Oct 13, 2014
:author: Markus Fasel
:contact: mfasel@lbl.gov
:organization: Lawrence Berkeley National Laboratory
'''
from Jobparameters import Jobparams, SimParams
import json as jsonhandler

class JSONtester(object):
    '''
    classdocs
    '''

    def __init__(self, filenameJob, filenameSim):
        '''
        Constructor
        '''
        # Write test Jobparams
        jparams = Jobparams()
        jparams.SetExecutable("aliroot")
        jparams.SetMacroLocation("/project/projectdirs/alice/mfasel/hopper/LHC13b2_efix_p1")
        jparams.SetOutputLocation("/project/projectdirs/alice/mfasel/hopper/LHC13b2_efix_p1")
        jparams.AddUserModulepath("/project/projectdirs/alice/hpc/modulefiles")
        jparams.LoadUserModule("AliRoot/v5-03-Rev-30")
        jsonstring = jparams.CreateJSONString()
        print jsonstring
        outputfile = open(filenameJob, "w")
        outputfile.write("%s\n" %(jsonstring))
        outputfile.close()
        
        # Write test Simparams
        sparams = SimParams()
        sparams.SetEnergy(7000)
        sparams.SetRunNumber(195344)
        sparams.SetNumberOfEvents(100)
        sparams.SetProcess("kPerugia2011")
        sparams.SetSeedpool("/project/projectdirs/alice/mfasel/seeds.db")
        sparams.AddOtherParam("Physics", "kEM")
        jsonstring = sparams.CreateJSONString()
        print jsonstring
        outputfile = open(filenameSim, "w")
        outputfile.write("%s\n" %(jsonstring))
        outputfile.close()
        
        # write combined
        config = {"jobparams":jparams.__dict__(), "simparams":sparams.__dict__()}
        outputfile = open("configCombined.json", "w")
        outputfile.write("%s\n"%( jsonhandler.dumps(config)))
        outputfile.close()
        
        # Read test Jobparams
        inputfile = open(filenameJob, "read")
        jsoninput = inputfile.readline()
        inputfile.close()
        rjobp = Jobparams.BuildFromJSON(jsoninput)
        rjobp.Print()
        
        # Read test Jobparams
        inputfile = open(filenameSim, "read")
        jsoninput = inputfile.readline()
        inputfile.close()
        rsimp = SimParams.BuildFromJSON(jsoninput)
        rsimp.Print()

        
def RunJSONTest():
    JSONtester("configJob.json","configSim.json")
    

if __name__ == "__main__":
    RunJSONTest()